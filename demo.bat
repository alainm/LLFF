@echo off
rem Use COLMAP to compute 6DoF camera poses
python imgs2poses.py data/testscene/

rem Create MPIs using pretrained network
python imgs2mpis.py data/testscene/ data/testscene/mpis_360 --height 360
    
rem Generate smooth path of poses for new views
@REM md data/testscene/outputs/
@REM python imgs2renderpath.py data/testscene/ data/testscene/outputs/test_path.txt --spiral
    
@REM cd cuda_renderer
@REM .\make.bat
@REM cd ..
    
rem Render novel views using input MPIs and poses
@REM .\cuda_renderer\cuda_renderer.exe data/testscene/mpis_360 data/testscene/outputs/test_path.txt data/testscene/outputs/test_vid.mp4 360 .8 18
