nas_path="/mnt/nas3_01_Data/Nerf_examples"
###################
# First test scene
###################
scene="rafa_playing"
nerf_ex_path=$nas_path/$scene/
height=434

# Use COLMAP to compute 6DoF camera poses
python imgs2poses.py $nerf_ex_path

# Create MPIs using pretrained network
python imgs2mpis.py \
    $nerf_ex_path \
   $nerf_ex_path/mpis_$height \
    --height $height
    
# Generate smooth path of poses for new views
mkdir $nerf_ex_path/llff_outputs/
python imgs2renderpath.py \
    $nerf_ex_path \
    $nerf_ex_path/llff_outputs/test_path.txt \
    --spiral
    
# Render novel views using input MPIs and poses
cuda_renderer/cuda_renderer \
    $nerf_ex_path/mpis_$height \
    $nerf_ex_path/llff_outputs/test_path.txt \
    $nerf_ex_path/llff_outputs/$scene"_vid.mp4" \
    $height .8 18

###################
# Second test scene
###################
scene="susana_test_front"
nerf_ex_path=$nas_path/$scene/

# Use COLMAP to compute 6DoF camera poses
python imgs2poses.py $nerf_ex_path

# Create MPIs using pretrained network
python imgs2mpis.py \
    $nerf_ex_path \
   $nerf_ex_path/mpis_$height \
    --height $height
    
# Generate smooth path of poses for new views
mkdir $nerf_ex_path/llff_outputs/
python imgs2renderpath.py \
    $nerf_ex_path \
    $nerf_ex_path/llff_outputs/test_path.txt \
    --spiral
    
# Render novel views using input MPIs and poses
cuda_renderer/cuda_renderer \
    $nerf_ex_path/mpis_$height \
    $nerf_ex_path/llff_outputs/test_path.txt \
    $nerf_ex_path/llff_outputs/$scene"_vid.mp4" \
    $height .8 18

